import * as React from "react";
import formatDuration from "format-duration";
import type { TrackElement } from "./types";

const Track: React.FC<TrackElement> = (track) => {
  return (
    <div className="rounded-box grid grid-cols-[auto,1fr,auto] gap-4 my-4 p-4 bg-neutral">
      <div className="flex items-center justify-center">
        #{track.disc}.{track.number}
      </div>
      <div className="flex flex-col">
        <span className="text-primary">{track.title}</span>
        <span className="text-xs py-1">{track.artists.join(", ") || "-"}</span>
      </div>
      <div className="flex items-center justify-center">
        {track.length ? formatDuration(track.length) : "-"}
      </div>
    </div>
  );
};

export default Track;
