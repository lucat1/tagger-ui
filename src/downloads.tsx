import * as React from "react";
import { useQuery } from "react-query";
import { DocumentIcon, FolderIcon } from "@heroicons/react/24/outline";
import prettyBytes from "pretty-bytes";
import { useNavigate, useParams } from "react-router";
import { Link } from "react-router-dom";

const Downloads: React.FC = () => {
  const navigate = useNavigate();
  const { path } = useParams();
  const { data: libraries } = useQuery(
    ["library"],
    async () => {
      const req = await fetch("http://localhost:3000/internal/library");
      return await req.json();
    },
    { suspense: true }
  );
  const { data: list } = useQuery(
    ["list", path],
    async () => {
      const req = await fetch(
        path
          ? `http://localhost:3000/internal/list?path=${path}`
          : "http://localhost:3000/internal/list"
      );
      return await req.json();
    },
    { suspense: true }
  );
  const [lib, setLib] = React.useState(0);

  const prev = path && path.split("/").slice(0, -1).join("/");
  return (
    <div className="flex h-screen items-center justify-center">
      <main>
        <div className="flex flex-row items-center py-4">
          <h1 className="text-xl">
            Importing from <span className="text-primary">{list.name}</span>
          </h1>
          <div className="flex flex-1 flex-row items-center justify-end">
            Library:
            <select
              className="select select-secondary mx-2"
              value={libraries[lib].name}
              onChange={({ target }) => setLib(target.selectedIndex)}
            >
              <option disabled>{libraries[lib].name}</option>
              {libraries.map((l, i) =>
                i != lib ? <option key={i}>{l.name}</option> : null
              )}
            </select>
          </div>
        </div>
        <div className="artboard artboard-horizontal phone-6 drop-shadow-md">
          <div className="overflow-x-auto">
            <table className="table w-full">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Size</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {path && (
                  <tr>
                    <th>
                      <FolderIcon className="w-5 h-5" />
                    </th>
                    <td>
                      <Link to={`/downloads/${prev}`}>..</Link>
                    </td>
                    <td>-</td>
                    <td>
                      <button className="btn" disabled={true}>
                        import
                      </button>
                    </td>
                  </tr>
                )}
                {list.entries.map((e, i) => (
                  <tr key={i}>
                    <th>
                      {e.type == "File" ? (
                        <DocumentIcon className="w-5 h-5" />
                      ) : (
                        <FolderIcon className="w-5 h-5" />
                      )}
                    </th>
                    <td>
                      {e.type == "Directory" ? (
                        <Link
                          className="link text-primary"
                          to={`/downloads/${e.path}`}
                        >
                          {e.name}
                        </Link>
                      ) : (
                        e.name
                      )}
                    </td>
                    <td>{prettyBytes(e.size)}</td>
                    <td>
                      <Link to={`/import?path=${e.path}&library=${lib}`}>
                        <button
                          className="btn"
                          disabled={e.type != "Directory"}
                        >
                          import
                        </button>
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Downloads;
