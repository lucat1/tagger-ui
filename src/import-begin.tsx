import * as React from "react";
import { useQuery } from "react-query";
import { useLocation, useNavigate } from "react-router";
import Loading from "./loading";

const ImportBegin: React.FC = () => {
  const navigate = useNavigate();
  const { search } = useLocation();
  const [path, library] = React.useMemo(() => {
    const params = new URLSearchParams(search);
    return [params.get("path"), parseInt(params.get("library") || "")];
  }, [search]);

  const { data, isLoading, isError } = useQuery(
    ["import-begin", path, library],
    async () => {
      const req = await fetch("http://localhost:3000/internal/import", {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ path, library }),
      });
      return await req.json();
    },
    {
      refetchOnWindowFocus: false,
      refetchOnReconnect: true,
    }
  );

  React.useEffect(() => {
    if (!isLoading && !isError) navigate(`/import/${data.id}`);
  }, [isLoading, isError]);

  return (
    <div className="flex flex-col h-screen items-center justify-center">
      {isLoading && (
        <>
          <Loading />
          <p className="pt-16 max-w-sm">
            The initial import can take some time due to requests to{" "}
            <span className="text-primary">MusicBrainz</span> and{" "}
            <span className="text-secondary">cover providers</span>. Please, do
            not move away from this page.
          </p>
        </>
      )}
      {isError && (
        <p className="pt-16 max-w-sm text-error">An error occoured.</p>
      )}
    </div>
  );
};

export default ImportBegin;
