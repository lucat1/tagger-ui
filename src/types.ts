export interface Import {
  id: string;
  path: string;
  library: number;
  release: Release;
  tracks: TrackElement[];
  search_results: SearchResult[];
  covers: [number, Cover][];
  selected: [string, number];
}

export interface Cover {
  provider: CoverProvider;
  url: string;
  width: number;
  height: number;
  title: string;
  artist: string;
}

export enum CoverProvider {
  CoverArtArchive = "CoverArtArchive",
  Deezer = "Deezer",
  Itunes = "Itunes",
}

export interface Release {
  title: string;
  artists: string[];
  media: null;
  discs: number;
  tracks: number;
  country: null;
  label: null;
  release_type: null;
  date: Date;
  original_date: null;
}

export interface SearchResult {
  rating: number;
  search_result: [FullRelease, FullTrack[]];
  mapping: number[];
}

export interface FullTrack {
  track: SearchResultTrack;
  artist_credit_track: ArtistCreditTrack[];
  artist_credit: ArtistCredit[];
  artist_track_relation: ArtistTrackRelation[];
  artist: Artist[];
}

export interface Artist {
  id: string;
  name: string;
  sort_name: string;
}

export interface ArtistCredit {
  id: string;
  join_phrase: string | null;
  artist_id: string;
}

export interface ArtistCreditTrack {
  artist_credit_id: string;
  track_id: string;
}

export interface ArtistTrackRelation {
  artist_id: string;
  track_id: string;
  relation_type: string;
  relation_value: string;
}

export interface SearchResultTrack {
  id: string;
  medium_id: string;
  title: string;
  length: number;
  number: number;
  genres: any[];
  format: null;
  path: null;
}

export interface FullRelease {
  release: SearchResultRelease;
  medium: Medium[];
  artist_credit_release: ArtistCreditRelease[];
  artist_credit: ArtistCredit[];
  artist: Artist[];
}

export interface SearchResultRelease {
  id: string;
  title: string;
  release_group_id: string | null;
  release_type: string;
  asin: number | null;
  country: string;
  label: string;
  catalog_no: string | null;
  status: string;
  date: string | null;
  original_date: string | null;
  script: string | null;
}

export interface ArtistCreditRelease {
  artist_credit_id: string;
  release_id: string;
}

export interface Medium {
  id: string;
  release_id: string;
  position: number;
  tracks: number;
  track_offset: number;
  format: string;
}

export interface TrackElement {
  title: string;
  artists: string[];
  length: number | null;
  disc: number;
  number: number;
}

const join_artists = (
  map: (ArtistCreditRelease | ArtistCreditTrack)[],
  credits: ArtistCredit[],
  artists: Artist[]
) => {
  return map
    .map((c) => credits.find((ac) => ac.id == c.artist_credit_id))
    .filter((ac): ac is ArtistCredit => !!ac)
    .map((ac) => ({
      join_phrase: ac.join_phrase,
      artist: artists.find((a) => a.id == ac.artist_id)!,
    }));
};

export const artists_for_release = (
  full_release: FullRelease
): { join_phrase: string | null; artist: Artist }[] =>
  join_artists(
    full_release.artist_credit_release.filter(
      (c) => (c.release_id = full_release.release.id)
    ),
    full_release.artist_credit,
    full_release.artist
  );

export const artists_for_track = (
  full_track: FullTrack
): { join_phrase: string | null; artist: Artist }[] =>
  join_artists(
    full_track.artist_credit_track.filter(
      (c) => (c.track_id = full_track.track.id)
    ),
    full_track.artist_credit,
    full_track.artist
  );
