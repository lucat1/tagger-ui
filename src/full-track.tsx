import * as React from "react";

import Track from "./track";
import { artists_for_track, FullRelease, FullTrack } from "./types";

const FullTrack: React.FC<{ track: FullTrack; release: FullRelease }> = ({
  track,
  release,
}) => {
  const medium = React.useMemo(
    () => release.medium.findIndex((med) => med.id == track.track.medium_id)!,
    [track.track.medium_id, release.medium]
  );
  return (
    <Track
      title={track.track.title}
      disc={medium + 1}
      number={track.track.number}
      length={track.track.length}
      artists={artists_for_track(track).map(({ artist }) => artist.name)}
    />
  );
};

export default FullTrack;
