import * as React from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import Downloads from "./downloads";
import ImportBegin from "./import-begin";
import Import from "./import";

const Redirect: React.FC<{ to: string }> = ({ to }) => {
  const navigate = useNavigate();
  React.useEffect(() => {
    navigate(to);
  }, []);
  return null;
};

const App: React.FC = () => (
  <Routes>
    <Route path="/" element={<Redirect to="/downloads" />} />
    <Route path="/downloads" element={<Downloads />} />
    <Route path="/downloads/:path" element={<Downloads />} />
    <Route path="/import" element={<ImportBegin />} />
    <Route path="/import/:id" element={<Import />} />
    <Route path="*" element={<h1>404</h1>} />
  </Routes>
);

export default App;
