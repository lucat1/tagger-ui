import * as React from "react";
import { useQueryClient, useQuery, useMutation } from "react-query";
import { useParams } from "react-router";

import Track from "./track";
import FullTrackComponent from "./full-track";
import {
  type Import,
  artists_for_release,
  FullTrack,
  FullRelease,
} from "./types";

const cmp = (a: number, b: number) => (a > b ? 1 : a == b ? 0 : -1);
const disc = (release: FullRelease, track: FullTrack) =>
  release.medium.findIndex((med) => med.id == track.track.medium_id)! + 1;

const Import: React.FC = () => {
  const { id } = useParams();
  const queryClient = useQueryClient();
  const { data } = useQuery(
    ["import", id],
    async (): Promise<Import> => {
      const req = await fetch(`http://localhost:3000/internal/import/${id}`);
      return await req.json();
    },
    {
      refetchOnWindowFocus: false,
      refetchOnReconnect: true,
      suspense: true,
    }
  );
  const update = useMutation({
    mutationFn: async ({
      type,
      value,
    }: { type: "MbId"; value: string } | { type: "Cover"; value: number }) => {
      const req = await fetch(`http://localhost:3000/internal/import/${id}`, {
        method: "PATCH",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ [type]: value }),
      });
      return await req.json();
    },
    onSuccess: (data) => {
      queryClient.setQueryData(["import", id], data);
    },
  });

  const proceed = useMutation({
    mutationFn: async () => {
      const req = await fetch(`http://localhost:3000/internal/import/${id}`, {
        method: "POST",
      });
      return await req.json();
    },
  });

  const cover = data!.covers[data!.selected[1]][1];
  const selected_search_result = data!.search_results.find(
    ({ search_result }) => search_result[0].release.id == data!.selected[0]
  );
  if (!selected_search_result) {
    // TODO
    return <h1>No results found</h1>;
  }

  return (
    <div className="flex flex-col container mx-auto px-4 py-6">
      <section className="grid grid-cols-1 lg:grid-cols-[auto,1fr,auto] gap-4 py-8">
        <div className="indicator">
          <span className="indicator-item indicator-bottom indicator-start badge badge-secondary">
            {cover.width}x{cover.height}
          </span>
          <span className="indicator-item indicator-top indicator-start badge badge-primary">
            {cover.provider}
          </span>
          <div className="carousel w-64 h-64 rounded-lg">
            <div className="carousel-item relative w-full">
              <img src={cover.url} className="w-full" />
              <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
                <button
                  onClick={(_) =>
                    update.mutate({
                      type: "Cover",
                      value: data!.selected[1] - 1,
                    })
                  }
                  className="btn btn-circle"
                  disabled={
                    update.isLoading ||
                    proceed.isLoading ||
                    data!.selected[1] <= 0
                  }
                >
                  ❮
                </button>
                <button
                  onClick={(_) =>
                    update.mutate({
                      type: "Cover",
                      value: data!.selected[1] + 1,
                    })
                  }
                  className="btn btn-circle"
                  disabled={
                    update.isLoading ||
                    proceed.isLoading ||
                    data!.selected[1] >= data!.covers.length - 1
                  }
                >
                  ❯
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="py-2">
          <h1 className="text-2xl py-2">Searching for</h1>
          <h2 className="text-xl">
            {data!.release.artists.map((a, i) => (
              <React.Fragment key={i}>
                <span className="text-primary">{a}</span>
                {i != data!.release.artists.length - 1 && ","}
              </React.Fragment>
            ))}{" "}
            - {data!.release.title} ({data!.release.tracks} tracks)
          </h2>
          <h2 className="text-2xl py-2">Importing as</h2>
          <h2 className="text-xl">
            {artists_for_release(selected_search_result.search_result[0]).map(
              (a, i) => (
                <React.Fragment key={i}>
                  <a
                    className="text-primary link"
                    href={`https://musicbrainz.org/artist/${a.artist.id}`}
                    target="_blank"
                  >
                    {a.artist.name}
                  </a>
                  {a.join_phrase || ""}
                </React.Fragment>
              )
            )}{" "}
            -{" "}
            <a
              className="link"
              href={`https://musicbrainz.org/release/${selected_search_result.search_result[0].release.id}`}
              target="_blank"
            >
              {selected_search_result.search_result[0].release.title}
            </a>{" "}
            ({data!.release.tracks} tracks)
          </h2>
        </div>
        <select
          className="select select-secondary mx-2"
          value={selected_search_result.search_result[0].release.id}
          disabled={update.isLoading || proceed.isLoading}
          onChange={({ target }) =>
            update.mutate({
              type: "MbId",
              value: target.value,
            })
          }
        >
          <option disabled>
            {selected_search_result.search_result[0].release.id}
          </option>
          {data!.search_results
            .map((sr) => sr.search_result)
            .map(([release], i) =>
              release.release.id != data!.selected[0] ? (
                <option key={i}>{release.release.id}</option>
              ) : null
            )}
        </select>
      </section>
      <main className="columns-1 lg:columns-2">
        <section>
          <h3 className="text-lg">Source tracks</h3>
          {data!.tracks
            .sort((a, b) =>
              a.disc != b.disc ? cmp(a.disc, b.disc) : cmp(a.number, b.number)
            )
            .map((track, i) => (
              <Track key={i} {...track} />
            ))}
        </section>
        <section>
          <h3 className="text-lg">Final tracks</h3>
          {selected_search_result.mapping
            .map((to) => selected_search_result.search_result[1][to])
            .sort((a, b) => {
              const a_disc = disc(selected_search_result.search_result[0], a),
                b_disc = disc(selected_search_result.search_result[0], b);
              return a_disc != b_disc
                ? cmp(a_disc, b_disc)
                : cmp(a.track.number, b.track.number);
            })
            .map((track, i) => (
              <FullTrackComponent
                key={i}
                track={track}
                release={selected_search_result.search_result[0]}
              />
            ))}
        </section>
      </main>
      <button
        className="btn self-end my-8"
        disabled={update.isLoading || proceed.isLoading}
      >
        Proceed
      </button>
    </div>
  );
};

export default Import;
